# Authenticator for Windows
This repository contains the official source code of the Authenticator for Windows app.
## Support
Since I was no longer maintaining this app, I have decided to make the app open source, so everyone would be able to clone, fork and compile his own version of Authenticator for Windows.
## Code quality
The codebase is, at the time of writing, already pretty old and could use some improvements. Still, it contains some useful pieces of code that I had to find out myself since there was not a lot to find on Google about UWP apps back then.

Be sure to grab pieces of code that you like and use them in your own app(s).
## Contributing
If you'd like to contribute, be sure to do so and create a pull request if you'd like to. I **CANNOT** guarantee that pull requests will be reviewed and/or merged.
## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)  
This source code is licensed under the MIT License. Be sure to read the [LICENSE.txt](LICENSE.txt) file before using the source code.